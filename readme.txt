Display Matrix toolbox

Purpose

The goal of this toolbox is to provide algorithms to display matrices.

 * dispmat_see : Pictures of a matrix and its pseudo-inverse. 
 * dispmat_show : Displays the sign of matrix entries.
 * dispmat_spec : Plots the eigenvalues of the matrix.
 * dispmat_gersh : Plots the Gershgoring circles of the matrix.
 * dispmat_plotnew : Displays a matrix of values with entries from A.
 * dispmat_plotconf : Configure a matrix plot of values with entries.
 * dispmat_plotgetcell : Returns the handle corresponding to the given cell.

See the overview in the help provided with this toolbox.

This module depends on the apifun module.

TODO

 * overload the printing system for the graphmat

Author

DIGITEO - Michael Baudin - 2008 - 2010

Licence

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



