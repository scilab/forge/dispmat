// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function dispmatlib = loaddispmatlib ()

    TOOLBOX_NAME = "dispmat"
    TOOLBOX_TITLE = "Dispmat"
    verbose_at_startup=%f

    mprintf("Start %s\n",TOOLBOX_TITLE);

    etc_tlbx  = get_absolute_file_path(TOOLBOX_NAME+".start");
    etc_tlbx  = getshortpathname(etc_tlbx);
    root_tlbx = strncpy( etc_tlbx, length(etc_tlbx)-length("\etc\") );

    //Load  functions library
    // =============================================================================

    if ( %t ) then
        if (verbose_at_startup) then
            mprintf("\tLoad macros\n");
        end
        pathmacros = pathconvert( root_tlbx ) + "macros" + filesep();
        dispmatlib  = lib(pathmacros);
    end

    // load gateways
    // =============================================================================

    if (%f) then
        mprintf("\tLoad gateways\n");
        ilib_verbose(0);
        exec( pathconvert(root_tlbx+"/sci_gateway/loader_gateway.sce",%f));
    end

    // Load and add help chapter
    // =============================================================================

    if or(getscilabmode() == ["NW";"STD"]) then
        mprintf("\tLoad help\n");
        path_addchapter = pathconvert(root_tlbx+"/jar");
        if ( isdir(path_addchapter) <> [] ) then
            add_help_chapter(TOOLBOX_TITLE, path_addchapter, %F);
        end
    end

    // add demos
    // =============================================================================
    if (%f) then
        if or(getscilabmode() == ["NW";"STD"]) then
            mprintf("\tLoad demos\n");
            pathdemos = pathconvert(root_tlbx+"/demos/dispmat.dem.gateway.sce",%f,%t);
            add_demo(TOOLBOX_TITLE,pathdemos);
        end
    end

endfunction

if ( isdef("dispmatlib") ) then
    warning("	Library is already loaded (""ulink(); clear dispmatlib;"" to unload.)");
    return;
end

dispmatlib = loaddispmatlib();
clear loaddispmatlib;
