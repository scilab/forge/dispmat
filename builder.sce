// ====================================================================
// Copyright INRIA 2008-2009
// Allan CORNET
// Simon LIPP
// Michael Baudin
// This file is released into the public domain
// ====================================================================

mode(-1);
lines(0);

function dispmatBuildToolbox()
    TOOLBOX_NAME = "dispmat";
    TOOLBOX_TITLE = "Display Matrix Toolbox";

    toolbox_dir = get_absolute_file_path("builder.sce");

    tbx_builder_macros(toolbox_dir);
    tbx_builder_help(toolbox_dir);
    tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
    tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);
endfunction

dispmatBuildToolbox();
clear dispmatBuildToolbox;
