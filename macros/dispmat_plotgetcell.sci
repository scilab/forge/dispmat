// Copyright (C) 2008 - 2010 - Michael Baudin

function cellij = dispmat_plotgetcell ( graphmat , i , j ) 
  //   Returns the handle corresponding to the given cell.
  // 
  // Calling Sequence
  //   cellij = dispmat_plotgetcell ( graphmat , i , j ) 
  //
  // Parameters
  //   graphmat : a graphical matrix, as created by dispmat_plotnew
  //   i : a 1x1 matrix of floating point integers, the row index of an entry of the graphical matrix
  //   j : a 1x1 matrix of floating point integers, the column index of an entry of the graphical matrix
  //   cellij : the uicontrol corresponding to the entry (i,j)
  //
  // Description
  //   Returns the handle corresponding to the given cell.
  //   This allows to configure the uicontrol object corresponding to the entry
  //   (i,j) in the matrix.
  //
  // Examples
  // A=[
  //           -9          11         -21          63        -252
  //           70         -69         141        -421        1684
  //         -575         575       -1149        3451      -13801
  //         3891       -3891        7782      -23345       93365
  //         1024       -1024        2048       -6144       24572
  // ];
  // graphmat = dispmat_plotnew ( A );
  // uientry = dispmat_plotgetcell ( graphmat , 2 , 3 );
  // uientry.String = string(123456);
  // // Pick a cell at random, color it at random
  // for k = 1:5
  //   i = grand(1,1,"uin",1,5);
  //   j = grand(1,1,"uin",1,5);
  //   uientry = dispmat_plotgetcell ( graphmat , i , j );
  //   uientry.BackgroundColor = grand(1,3,"def");
  // end
  // // Take some time to see the matrix...
  // // ... now close
  // close ( graphmat.figure );
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //
  // 

  [lhs, rhs] = argn()
  apifun_checkrhs ( "dispmat_plotgetcell" , rhs , 3 )
  apifun_checklhs ( "dispmat_plotgetcell" , lhs , 1 )
  //
  apifun_checktype ( "dispmat_plotgetcell" , graphmat , "graphmat" , 1 , "T_DMPLOT" )
  apifun_checktype ( "dispmat_plotgetcell" , i , "i" , 2 , "constant" )
  apifun_checktype ( "dispmat_plotgetcell" , j , "j" , 3 , "constant" )
  //
  apifun_checkgreq ( "dispmat_plotgetcell" , i , "i" , 2 , 1 )
  apifun_checkgreq ( "dispmat_plotgetcell" , j , "j" , 3 , 1 )
  //
  if ( i - floor(i) <> 0 )
        error(msprintf(gettext ( "%s: Wrong value for input argument #%d: variable %d is equal to %s, which is not an integer.") , "dispmat_plotgetcell" , 2 , "i" , string(i) ))
  end
  if ( j - floor(j) <> 0 )
        error(msprintf(gettext ( "%s: Wrong value for input argument #%d: variable %d is equal to %s, which is not an integer.") , "dispmat_plotgetcell" , 3 , "j" , string(j) ))
  end
  //
  ncols = size(graphmat.A,"r")
  cellij = graphmat.uicontrols((i-1)*ncols + j)
endfunction

